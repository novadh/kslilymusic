# ksLilyMusic

## Overview

A LaTeX style file to typeset music engravings made with [Lilypond](http://lilypond.org) in the LaTeX document.

## How to use

put

```
\usepackage{kslilypond}
```

in preamble of your document.

And put

```
\begin{kslily}
<lilypond music notations>
\end{kslily}
```

in the body of the document.

## Caveat

This style utilizes the external shell commands. So you should compile the document with `-shell-escape` option.

* The `lilypond` must be installed and can be called from the shell command line.
* The `pdfcrop` utility shipped in TeXLive distribution, should be accessible.

# License

It's free. Either LPPL or just public domain, as you like.

